# Quadflow : A Java-based quadtree implementation

Quadflow is a simple quadtree implementation for Java. It was used for the *Crenaud UltraViolet* project (see https://gitlab.com/Aethor/crenaud-ultraviolet-s-edition).


# How to use

Javadoc is available on all main methods of the quadtree. You can also see an example in *src/main/java/example*. It uses JavaFX to display an example of the quadtree in action !


# Maven import

I didn't set up anything in maven central, but you can still use https://jitpack.io to include this library in yout project :

```xml
    ...
    <repositories>
        ...
        <repository>
            <id>jitpack.io</id>
            <url>https://jitpack.io</url>
        </repository>
    </repositories>
    ...
    <dependencies>
        ...
        <dependency>
            <groupId>com.gitlab.Aethor</groupId>
            <artifactId>quadflow</artifactId>
            <version>-SNAPSHOT</version>
        </dependency>
    </dependencies>
```