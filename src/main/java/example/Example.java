package example;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;
import quadtree.Quadtree;
import quadtree.Spatialisable;

import java.util.LinkedList;

public class Example extends Application {

    @FXML private Canvas mainCanvas;

    private LinkedList<SpatialEntityExample> entities;
    private Quadtree quadtree;

    public static void main(String[] args){
        launch(args);
    }

    public void start(Stage stage) throws Exception {
        this.quadtree = new Quadtree(
                new Rectangle2D(
                        0,
                        0,
                        400,
                        400
                ),
                3,
                5
        );
        this.entities = new LinkedList<SpatialEntityExample>();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/window.fxml"));
        loader.setController(this);
        Parent root = loader.load();
        Scene scene = new Scene(root, 600, 400);
        stage.setScene(scene);

        final GraphicsContext gc = this.mainCanvas.getGraphicsContext2D();


        new AnimationTimer() {
            long currentTime = 0;
            public void handle(long l) {

                float delta = (float) (l - currentTime) / 1000000000f;
                //clear
                gc.clearRect(0, 0, mainCanvas.getWidth(), mainCanvas.getHeight());

                //move entities
                for (SpatialEntityExample entity : entities){
                    if (entity.getCollider().getMinY() <= 10 || entity.getCollider().getMaxY() >= quadtree.square.getHeight() - 10)
                        entity.speed = new Point2D(entity.speed.getX(), - entity.speed.getY());
                    if (entity.getCollider().getMinX() <= 10 || entity.getCollider().getMaxX() >= quadtree.square.getHeight() - 10)
                        entity.speed = new Point2D(- entity.speed.getX(), entity.speed.getY());
                    quadtree.move(entity, new Point2D(
                            entity.getCollider().getMinX() + entity.speed.getX() * delta,
                            entity.getCollider().getMinY() + entity.speed.getY() * delta
                    ));
                }
                currentTime = l;

                //draw entities
                for (SpatialEntityExample entity : entities){

                    Rectangle2D rec = entity.getCollider();

                    if (quadtree.isColliding(entity))
                        gc.setFill(Color.RED);
                    else
                        gc.setFill(Color.BLACK);

                    gc.fillRect(
                            (double) rec.getMinX(),
                            (double) rec.getMinY(),
                            (double) rec.getWidth(),
                            (double) rec.getHeight());

                    gc.setStroke(Color.BLACK);
                    gc.fillArc((double) rec.getMinX() + rec.getWidth()/2, rec.getMinY() + rec.getHeight()/2, (double) 2, (double) 2, (double) 0, 360, ArcType.ROUND);

                }

                //draw quadtree
                this.drawQuadtree(quadtree);
            }
            private void drawQuadtree(Quadtree quadtree){
                LinkedList<Quadtree> children = quadtree.getChildren();
                if (children.size() == 0)
                    return;

                gc.strokeLine(quadtree.square.getMinX(), quadtree.getCenter().getY(),
                        quadtree.square.getMaxX(), quadtree.getCenter().getY());
                gc.strokeLine(quadtree.getCenter().getX(), quadtree.square.getMinY(),
                        quadtree.getCenter().getX(), quadtree.square.getMaxY());
                for (Quadtree child : children){
                    drawQuadtree(child);
                }

            }
        }.start();

        stage.show();
    }

    @FXML protected void onMouseClick(MouseEvent event){
        if (event.getButton() == MouseButton.PRIMARY){
            SpatialEntityExample entity = new SpatialEntityExample(new Rectangle2D(event.getX(),  event.getY(), 20, 20));
            this.quadtree.insert(entity);
            this.entities.add(entity);
        } else if (event.getButton() == MouseButton.SECONDARY){
            Spatialisable entity = this.quadtree.getClosestEntity(new Point2D((float) event.getX(), (float) event.getY()));
            this.quadtree.delete(entity);
            this.entities.remove((SpatialEntityExample) entity);
        }

    }
}
