package example;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import quadtree.Spatialisable;

public class SpatialEntityExample implements Spatialisable {

    public Rectangle2D rectangle;
    public Point2D speed;

    public SpatialEntityExample(Rectangle2D rectangle){
        this.speed = new Point2D((float) (Math.random() - 0.5f) * 200, (float) (Math.random() - 0.5f) * 200);
        this.rectangle = rectangle;
    }

    public Rectangle2D getCollider() {
        return this.rectangle;
    }

    public Point2D getCenter(){
        return new Point2D(
                this.rectangle.getMinX() + this.rectangle.getWidth()/2,
                this.rectangle.getMinY() + this.rectangle.getHeight()/2
        );
    }

    public void setPosition(Point2D newPosition){
        this.rectangle = new Rectangle2D(
                newPosition.getX(),
                newPosition.getY(),
                this.rectangle.getWidth(),
                this.rectangle.getHeight()
        );
    }

}
