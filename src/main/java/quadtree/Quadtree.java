package quadtree;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;


public class Quadtree implements Iterable<Spatialisable>{


    private LinkedList<Quadtree> children;

    public Rectangle2D square;

    private int maxDepth;

    private int maxSpatialEntities;
    private LinkedList<Spatialisable> spatialEntities;

    /**
     *
     * @param maxSpatialEntities max spatial entities comprised in each node of the tree. When this number is exceeded, the tree will split.
     * @param square
     * @param maxDepth maximum splits of the tree
     */
    public Quadtree(Rectangle2D square, int maxSpatialEntities, int maxDepth){

        if (maxDepth <= 0) {
            System.err.println("quadflow : could not initialize quadtree : maxDepth must be positive");
            System.exit(1);
        }
        this.maxDepth = maxDepth;

        if (maxSpatialEntities <= 0){
            System.err.println("quadflow : could not initialize quadtree : maxSpatialEntities must be > 0");
            System.exit(1);
        }
        this.maxSpatialEntities = maxSpatialEntities;

        if (square.getWidth() != square.getHeight()) {
            System.err.println("quadflow : could not initialize quadtree : square width must be equal to its height");
            System.exit(1);
        }
        this.square = square;

        this.spatialEntities = new LinkedList<Spatialisable>();
        this.children = new LinkedList<Quadtree>();
    }

    /**
     * Delete the specified entity from the tree. Splits of the tree may be removed.
     * @param entity
     */
    public void delete(Spatialisable entity){
        if (entity == null)
            return;

        if (this.spatialEntities.contains(entity)){
            this.spatialEntities.remove(entity);
            return;
        }
        if (this.children.isEmpty())
            return;

        for (Quadtree child : this.children){
            if (child.intersect(entity)) {
                child.delete(entity);
            }
        }

        if (this.areChildrenEmpty())
            this.children.clear();
    }

    /**
     * Insert the specified entity in the tree. May lead to a split of the tree.
     * @param entity
     */
    public void insert(Spatialisable entity){

        //if this should be a leaf node
        if ((this.spatialEntities.size() < maxSpatialEntities && this.children.isEmpty()) || this.maxDepth == 1) {
            this.spatialEntities.add(entity);
            return;
        }

        // split part
        if (this.children.isEmpty()) {
            this.children.add(new Quadtree(
                    new Rectangle2D(
                            this.square.getMinX(),
                            this.square.getMinY(),
                            this.square.getWidth()/2,
                            this.square.getHeight()/2
                    ),
                    this.maxSpatialEntities,
                    this.maxDepth - 1)
            );
            this.children.add(new Quadtree(
                    new Rectangle2D(
                            this.square.getMinX() + this.square.getWidth()/2,
                            this.square.getMinY(),
                            this.square.getWidth()/2,
                            this.square.getHeight()/2
                    ),
                    this.maxSpatialEntities,
                    this.maxDepth - 1)
            );
            this.children.add(new Quadtree(
                    new Rectangle2D(
                            this.square.getMinX(),
                            this.square.getMinY() + this.square.getHeight()/2,
                            this.square.getWidth()/2,
                            this.square.getHeight()/2
                    ),
                    this.maxSpatialEntities,
                    this.maxDepth - 1)
            );
            this.children.add(new Quadtree(
                    new Rectangle2D(
                            this.square.getMinX() + this.square.getWidth()/2,
                            this.square.getMinY() + this.square.getHeight()/2,
                            this.square.getWidth()/2,
                            this.square.getHeight()/2
                    ),
                    this.maxSpatialEntities,
                    this.maxDepth - 1)
            );
        }

        // move all entities of this node in children
        for (Spatialisable currentEntity : this.spatialEntities)
            this.insertInChildren(currentEntity);
        this.spatialEntities.clear();

        // insert given entity
        this.insertInChildren(entity);
    }

    /**
     * Move the specified entity at the specified position, updating the tree
     * todo : enhance performance with better algorithm
     * @param entity the entity to move
     * @param newPosition the position which should be set
     */
    public void move(Spatialisable entity, Point2D newPosition){
        if (this.intersect(newPosition)) {
            this.delete(entity);
            entity.setPosition(newPosition);
            this.insert(entity);
        }
    }

    /**
     * Insert the given entity in all children intersecting with the given entity
     * @param entity
     */
    private void insertInChildren(Spatialisable entity){
        for (Quadtree child : this.children){
            if (child.intersect(entity)){
                child.insert(entity);
            }
        }
    }

    /**
     * Check if the given entity is colliding with another one in the tree.
     * @param entity
     * @return
     */
    public boolean isColliding(Spatialisable entity){
        for (Spatialisable other : this.spatialEntities){
            if (other != entity && other.getCollider().intersects(entity.getCollider()))
                return true;
        }
        if (!this.children.isEmpty()){
            for (Quadtree child : this.children){
                if (child.intersect(entity)){
                    if (child.isColliding(entity))
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * Return all entities colliding with the given entity
     * @implNote untested
     * @param entity
     * @return
     */
    public ArrayList<Spatialisable> getCollidingObjects(Spatialisable entity){
        ArrayList<Spatialisable> collidingObjects = new ArrayList<Spatialisable>();
        for (Spatialisable other : this.spatialEntities){
            if (other != entity && other.getCollider().intersects(entity.getCollider()))
                collidingObjects.add(other);
        }
        if (!this.children.isEmpty()){
            for (Quadtree child : this.children){
                if (child.intersect(entity)){
                    collidingObjects.addAll(child.getCollidingObjects(entity));
                }
            }
        }
        return new ArrayList<>(new HashSet<>(collidingObjects));
    }

    /**
     * Return all entities colliding with the given rectangle
     * @implNote untested
     * @param rectangle2D
     * @return
     */
    public ArrayList<Spatialisable> getCollidingObjects(Rectangle2D rectangle2D){
        ArrayList<Spatialisable> collidingObjects = new ArrayList<Spatialisable>();
        for (Spatialisable other : this.spatialEntities){
            if (other.getCollider().intersects(rectangle2D))
                collidingObjects.add(other);
        }
        if (!this.children.isEmpty()){
            for (Quadtree child : this.children){
                if (child.intersect(rectangle2D)){
                    collidingObjects.addAll(child.getCollidingObjects(rectangle2D));
                }
            }
        }
        return new ArrayList<>(new HashSet<>(collidingObjects));
    }

    /**
     * Return the closest entity to the given position
     * todo : better algorithm
     * @param position
     * @return
     */
    public Spatialisable getClosestEntity(final Point2D position){
        Spatialisable closestEntity = this.getLocalClosestEntity(position);
        if (closestEntity != null)
            return closestEntity;

        LinkedList<Spatialisable> closestEntities = new LinkedList<>();
        for (Quadtree child : this.children){
            if (child.intersect(position)){
                Spatialisable childClosestEntity = child.getClosestEntity(position);
                if (childClosestEntity != null)
                    closestEntities.add(childClosestEntity);
            }
        }
        return Quadtree.getLocalClosestEntity(closestEntities, position);
    }

    /**
     * Return the closest entity to the given position in this node only (this wont check children nodes)
     * @param position
     * @return
     */
    public Spatialisable getLocalClosestEntity(Point2D position){
        return Quadtree.getLocalClosestEntity(this.spatialEntities, position);
    }

    private static Spatialisable getLocalClosestEntity(LinkedList<Spatialisable> entities, Point2D position){
        if (!entities.isEmpty()){
            double minDist = entities.get(0).getCenter().distance(position);
            Spatialisable closestEntity = entities.get(0);
            for (Spatialisable entity : entities.subList(1, entities.size())){
                double dist = entity.getCenter().distance(position);
                if (dist < minDist){
                    minDist = dist;
                    closestEntity = entity;
                }
            }
            return closestEntity;
        }
        return null;
    }

    private boolean intersect(Point2D position){
        return this.square.contains(position);
    }

    private boolean intersect(Spatialisable entity){
        return entity.getCollider().intersects(this.square);
    }

    private boolean intersect(Rectangle2D rectangle2D){return this.square.intersects(rectangle2D);}

    private boolean areChildrenEmpty(){
        if (this.children.isEmpty())
            return true;
        for (Quadtree child : this.children){
            if (!child.spatialEntities.isEmpty() || !child.areChildrenEmpty())
                return false;
        }
        return true;
    }

    private boolean isInChildren(Spatialisable entity){
        for (Quadtree child : this.children){
            if (child.spatialEntities.contains(entity)){
                return true;
            }
        }
        return false;
    }

    public LinkedList<Quadtree> getChildren(){
        return this.children;
    }

    public Point2D getCenter(){
        return new Point2D(
                this.square.getMinX() + this.square.getWidth()/2,
                this.square.getMinY() + this.square.getHeight()/2
        );
    }

    public LinkedList<Spatialisable> getSpatialEntities(){
        if (this.children.isEmpty()){
            return this.spatialEntities;
        }

        LinkedList<Spatialisable> entities = new LinkedList<>();
        for (Quadtree child : this.children){
            entities.addAll(child.getSpatialEntities());
        }
        return new LinkedList<Spatialisable>(new HashSet<>(entities));
    }

    public Iterator<Spatialisable> iterator(){
        return new QuadtreeIterator(this);
    }
}
