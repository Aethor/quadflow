package quadtree;

import quadtree.Quadtree;

import java.util.Iterator;
import java.util.LinkedList;

public class QuadtreeIterator implements Iterator<Spatialisable> {

    private LinkedList<Spatialisable> list;
    private Spatialisable current;

    public QuadtreeIterator(Quadtree quadtree){
        this.list = quadtree.getSpatialEntities();
        this.current = this.list.getFirst();
    }

    @Override
    public boolean hasNext() {
        return this.current == null;
    }

    @Override
    public Spatialisable next() {
        this.current = this.list.get(this.list.indexOf(this.current) + 1);
        return this.list.get(this.list.indexOf(this.current) - 1);
    }
}
