package quadtree;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

public interface Spatialisable {
    public Rectangle2D getCollider();
    public Point2D getCenter();
    public void setPosition(Point2D newCollider);
}
